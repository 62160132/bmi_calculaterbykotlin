package com.example.bmi_calculatebykotlin

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.example.bmi_calculatebykotlin.databinding.ActivityMainBinding
import kotlin.math.ceil
import kotlin.math.pow

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.calculateButton.setOnClickListener { calculateBMI() }
        binding.weight.setOnKeyListener{view, keycode, _ -> handleKeyEvent(view, keycode)}
        binding.height.setOnKeyListener{view, keycode, _ -> handleKeyEvent(view, keycode)}
    }

    private fun calculateBMI() {
        val stringInWeight = binding.weightBox.text.toString()
        val stringInHeight = binding.heightBox.text.toString()
        val weight = stringInWeight.toDoubleOrNull()
        val height = stringInHeight.toDoubleOrNull()
        if(weight == null || weight == 0.0 || height == null || height == 0.0){
            return
        }
        var bmi = (weight/(height/100).pow(2))
        bmi = ceil(bmi)
        binding.bmiResult.text = getString(R.string.bmi_display, bmi.toString())
        var type = if (bmi < 18.5){
            "Mild Thinness"
        }else if(bmi in 18.5..24.9){
            "Normal"
        }else if(bmi in 25.0..29.9){
            "Overweight"
        }else{
            "Obese Class"
        }
        binding.typeResult.text = type
    }

    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }
}